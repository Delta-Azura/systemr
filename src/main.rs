// imports
use notify_rust::Notification;
use std::process::Command;
use std::{thread, time};

fn notification() {
Notification::new()
    .summary("EMERGENCY RESTART")
    .body("You need to restart your system : HIGH")
    .icon("firefox")
    .show();
}

fn main() {
    loop {
        let output_l = Command::new("/usr/bin/pacman")
            .arg("-Q").arg("linux")
            .output().unwrap_or_else(|e| {
                panic!("failed to execute process: {}", e)
     });
        let linux_package = String::from_utf8_lossy(&output_l.stdout);
        println!("{}", linux_package);

      // systemd

        let output_s = Command::new("pacman")
            .arg("-Q").arg("systemd")
            .output().unwrap_or_else(|e| {
                panic!("failed to execute process: {}", e)
    });
        let systemd_package = String::from_utf8_lossy(&output_s.stdout);
        println!("{}", systemd_package);

        
        let ten_millis = time::Duration::from_millis(100000);
        let now = time::Instant::now();
        thread::sleep(ten_millis);

        let out = Command::new("pacman")
            .arg("-Q").arg("linux")
            .output().unwrap_or_else(|e| {
                panic!("failed to execute process: {}", e)
        });
        let linux_package_verif = String::from_utf8_lossy(&out.stdout);
        println!("{}", linux_package_verif);

        let output_z = Command::new("pacman")
            .arg("-Q").arg("systemd")
            .output().unwrap_or_else(|e| {
                panic!("failed to execute process: {}", e)
        });
        let systemd_package_verif = String::from_utf8_lossy(&output_z.stdout);
        println!("{}", systemd_package_verif);

        if systemd_package == systemd_package_verif {
            println!("Pas de changement au niveau de systemD");
        } else {
            notification();
        }

        if linux_package == linux_package_verif {
            println!("Pas de changement au niveau du kernel");
        } else {
            notification();
        }

    }
}
