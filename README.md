# Systemr
Systemr est un utilitaire qui vous préviens lorsque vous devez redémarrer votre arch suite à une maj du noyau ou de systemd.

# Comment le mettre en place ? 
Il faut cloner le dépôt https://gitlab.com/Delta-Azura/systemr.git puis copier les fichiers systemr.service et systemr.timer dans ~/.config/systemd/user et de créer un répertoire dans lequel vous mettrez le fichier systemr.sh.
Une fois que c'est fait, tapez la commande systemctl --user start systemr && systemctl --user enable systemr. Si vous souhaitez vérifier que systemr est actif : systemctl --user status systemr. Enfin veillez à ce que rust soit installé !
